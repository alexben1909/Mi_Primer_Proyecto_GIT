1.1.1. Definici�n de GIT:
Es un software de control de versiones dise�ado por Linus Torvalds (dise�o el Kernel de
Linux), creado para mantener la eficiencia y la confiabilidad de versiones de aplicaciones
cuando se tiene un gran n�mero de archivos de c�digo fuente. Git cada vez que se inicia
genera un repositorio local de versiones de archivos. Git se ha convertido en un sistema
de control de versiones con funcionalidad plena. Existen muchos proyectos de relevancia
que ya usan Git, en particular, el grupo de programaci�n del n�cleo Linux.